import 'package:dio/dio.dart';
import 'package:football_app/core/utils/app_constants.dart';

import '../../../../core/api/dio_helper.dart';
import '../../../../core/models/league_model.dart';
import '../../../../core/models/soccer_fixture_model.dart';

abstract class SoccerDataSource {
  Future<List<LeagueModel>> getLeagues();

  Future<List<SoccerFixtureModel>> getDayFixtures({
    required String date,
  });
}

class SoccerDataSourceImpl implements SoccerDataSource {
  final DioHelper dioHelper;

  SoccerDataSourceImpl({required this.dioHelper});

  @override
  Future<List<SoccerFixtureModel>> getDayFixtures(
      {required String date}) async {
    try {
      final response = await dioHelper
          .get(url: AppConstants.FIXTURES, queryParams: {"date": date});
      return _getResult(response);
    } catch (error) {
      rethrow;
    }
  }

  @override
  Future<List<LeagueModel>> getLeagues() async {
    try {
      final response = await dioHelper.get(url: AppConstants.LEAGUES);
      List<dynamic> result = response.data["response"];
      List<LeagueModel> leagues = List<LeagueModel>.from(result.map(
        (item) => LeagueModel.fromJson(item),
      ));
      return leagues;
    } catch (error) {
      rethrow;
    }
  }
}

List<SoccerFixtureModel> _getResult(Response response) {
  List<dynamic> result = response.data["response"];
  List<SoccerFixtureModel> fixtures = List<SoccerFixtureModel>.from(result.map(
    (fixture) => SoccerFixtureModel.fromJson(fixture),
  ));
  return fixtures;
}
