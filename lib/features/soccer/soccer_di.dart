import 'package:football_app/di.dart';

import '../../core/api/dio_helper.dart';
import 'data/datasources/soccer_data_source.dart';
import 'data/repositories/soccer_repository_impl.dart';
import 'domain/use_cases/day_fixtures_usecase.dart';
import 'domain/use_cases/leagues_usecase.dart';
import 'presentation/cubit/soccer_cubit.dart';

void initSoccer() {
  sl.registerLazySingleton<SoccerDataSourceImpl>(
    () => SoccerDataSourceImpl(dioHelper: sl<DioHelper>()),
  );
  sl.registerLazySingleton<SoccerRepositoryImpl>(
    () => SoccerRepositoryImpl(
      soccerDataSource: sl<SoccerDataSourceImpl>(),
    ),
  );
  sl.registerLazySingleton<DayFixturesUseCase>(
    () => DayFixturesUseCase(
      soccerRepository: sl<SoccerRepositoryImpl>(),
    ),
  );
  sl.registerLazySingleton<LeaguesUseCase>(
    () => LeaguesUseCase(
      soccerRepository: sl<SoccerRepositoryImpl>(),
    ),
  );

  sl.registerFactory<SoccerCubit>(
    () => SoccerCubit(
      dayFixturesUseCase: sl<DayFixturesUseCase>(),
      leaguesUseCase: sl<LeaguesUseCase>(),
    ),
  );
}
