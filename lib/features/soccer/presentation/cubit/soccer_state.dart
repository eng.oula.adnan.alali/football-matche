import 'package:football_app/core/domain/entities/soccer_fixture.dart';

import '../../../../core/domain/entities/league.dart';

abstract class SoccerStates {}

class ScoreInitial extends SoccerStates {}

class SoccerLeaguesLoading extends SoccerStates {}

class SoccerLeaguesSuccess extends SoccerStates {
  final List<League> leagues;

  SoccerLeaguesSuccess(this.leagues);
}

class SoccerLeaguesFailure extends SoccerStates {
  final String message;

  SoccerLeaguesFailure(this.message);
}

class SoccerFixturesLoading extends SoccerStates {}

class SoccerFixturesSuccess extends SoccerStates {
  final List<SoccerFixture> fixtures;

  SoccerFixturesSuccess(this.fixtures);
}

class SoccerFixturesFailure extends SoccerStates {
  final String message;

  SoccerFixturesFailure(this.message);
}
