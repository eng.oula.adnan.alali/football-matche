import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../core/domain/entities/league.dart';
import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/usecase/usecase.dart';
import '../../../../core/utils/app_constants.dart';
import '../../domain/entities/league_of_fixture.dart';
import '../../domain/use_cases/day_fixtures_usecase.dart';
import '../../domain/use_cases/leagues_usecase.dart';
import '../screens/soccer_screen.dart';
import 'soccer_state.dart';

class SoccerCubit extends Cubit<SoccerStates> {
  final DayFixturesUseCase dayFixturesUseCase;
  final LeaguesUseCase leaguesUseCase;

  SoccerCubit({
    required this.dayFixturesUseCase,
    required this.leaguesUseCase,
  }) : super(ScoreInitial());

  Widget screens = const SoccerScreen();

  String titles = "Fixtures";

  List<League> filteredLeagues = [];

  Future<List<League>> getLeagues() async {
    emit(SoccerLeaguesLoading());
    filteredLeagues = [];
    final leagues = await leaguesUseCase(NoParams());
    leagues.fold(
      (left) => emit(SoccerLeaguesFailure(left.message)),
      (right) {
        for (League league in right) {
          if (AppConstants.availableLeagues.contains(league.id)) {
            filteredLeagues.add(league);
            AppConstants.leaguesFixtures
                .putIfAbsent(league.id, () => LeagueOfFixture(league: league));
          }
        }
        emit(SoccerLeaguesSuccess(filteredLeagues));
      },
    );
    return filteredLeagues;
  }

  List<SoccerFixture> dayFixtures = [];

  Future<List<SoccerFixture>> getFixtures() async {
    emit(SoccerFixturesLoading());
    String date = DateFormat("yyyy-MM-dd").format(DateTime.utc(2022, 9, 27));
    final fixtures = await dayFixturesUseCase(date);
    List<SoccerFixture> filteredFixtures = [];
    fixtures.fold(
      (left) => emit(SoccerFixturesFailure(left.message)),
      (right) {
        AppConstants.leaguesFixtures.forEach((key, value) {
          value.fixtures.clear();
        });
        for (SoccerFixture fixture in right) {
          if (AppConstants.availableLeagues
              .contains(fixture.fixtureLeague.id)) {
            filteredFixtures.add(fixture);
            AppConstants.leaguesFixtures[fixture.fixtureLeague.id]!.fixtures
                .add(fixture);
          }
          dayFixtures = filteredFixtures;
        }
        emit(SoccerFixturesSuccess(filteredFixtures));
      },
    );
    return filteredFixtures;
  }
}
