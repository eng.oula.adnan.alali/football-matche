import 'package:flutter/material.dart';
import 'package:football_app/core/utils/app_route.dart';

import '../../../../core/media_query.dart';
import '../../../../core/utils/app_colors.dart';

class BlockAlert extends StatelessWidget {
  final String message;

  const BlockAlert({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Icon(
          Icons.error_outline,
          color: AppColors.red,
          size: 90,
        ),
        const SizedBox(height: 10),
        Text(
          message,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        const SizedBox(height: 10),
        SizedBox(
          width: context.width / 2,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: AppColors.blue,
            ),
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushReplacementNamed(Routes.soccerLayout);
            },
            child: const Text("Reload"),
          ),
        ),
      ],
    );
  }
}

void buildBlockAlert({required BuildContext context, required String message}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (_) => AlertDialog(
      content: BlockAlert(message: message),
    ),
  );
}
