import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:football_app/core/utils/app_colors.dart';
import 'package:football_app/features/soccer/presentation/cubit/soccer_cubit.dart';
import 'package:football_app/features/soccer/presentation/cubit/soccer_state.dart';

class SoccerLayout extends StatelessWidget {
  const SoccerLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SoccerCubit, SoccerStates>(
      listener: (context, state) {},
      builder: (context, state) {
        SoccerCubit cubit = context.read<SoccerCubit>();
        return Scaffold(
          appBar: AppBar(
            title: Text(
              cubit.titles,
              style: TextStyle(color: AppColors.white),
            ),
            backgroundColor: AppColors.black,
            elevation: 0.0,
            leading: Icon(
              Icons.sports_basketball,
              color: AppColors.white,
            ),
          ),
          body: cubit.screens,
        );
      },
    );
  }
}
