import 'package:dartz/dartz.dart';

import '../../../../core/domain/entities/league.dart';
import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/error/error_handler.dart';

abstract class SoccerRepository {
  Future<Either<Failure, List<League>>> getLeagues();

  Future<Either<Failure, List<SoccerFixture>>> getDayFixtures({
    required String date,
  });
}
