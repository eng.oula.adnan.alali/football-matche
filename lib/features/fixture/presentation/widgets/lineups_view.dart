import 'package:flutter/material.dart';

import '../../../../core/utils/app_assets.dart';
import '../../../../core/utils/app_colors.dart';
import '../../domain/entities/lineups.dart';
import 'items_not_available.dart';
import 'teams_lineups.dart';

///todo
class LineupsView extends StatelessWidget {
  final List<Lineup> lineups;

  const LineupsView({Key? key, required this.lineups}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return lineups.isNotEmpty
        ? Column(
            children: [
              buildTeamHeader(context: context, lineup: lineups[0]),
              Container(
                width: double.infinity,
                height: 625,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(AppAssets.playground),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 15,
                    horizontal: 15,
                  ),
                  child: TeamsLineups(lineups: lineups),
                ),
              ),
              buildTeamHeader(context: context, lineup: lineups[1]),
            ],
          )
        : const ItemsNotAvailable(
            icon: Icons.sports_golf_rounded,
            message: 'Lineups not available',
          );
  }

  Widget buildTeamHeader(
      {required BuildContext context, required Lineup lineup}) {
    return Container(
      color: AppColors.green,
      padding: const EdgeInsetsDirectional.all(5),
      child: Row(
        children: [
          Image(
            fit: BoxFit.cover,
            width: 35,
            height: 35,
            image: NetworkImage(lineup.team.logo),
          ),
          const SizedBox(width: 10),
          Text(
            lineup.team.name,
            style: TextStyle(
              fontSize: 20,
              color: AppColors.white,
            ),
          ),
          const Spacer(),
          Text(
            lineup.formation,
            style: const TextStyle(color: Colors.white, fontSize: 20),
          ),
          const SizedBox(width: 10),
        ],
      ),
    );
  }
}
