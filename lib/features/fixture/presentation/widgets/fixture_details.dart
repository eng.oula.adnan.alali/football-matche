import 'package:flutter/material.dart';

import '../../../../core/domain/entities/soccer_fixture.dart';
import '../../../../core/utils/app_colors.dart';
import '../../../soccer/presentation/screens/soccer_screen.dart';
import 'view_team.dart';

class FixtureDetails extends StatelessWidget {
  final SoccerFixture soccerFixture;

  const FixtureDetails({Key? key, required this.soccerFixture})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      decoration: BoxDecoration(gradient: getGradientColor(soccerFixture)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            child: Text(
              soccerFixture.fixtureLeague.name,
              style: TextStyle(
                color: AppColors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: ViewTeam(team: soccerFixture.teams.home),
              ),
              (soccerFixture.fixture.status.elapsed != null)
                  ? Expanded(child: buildFixtureResult(context))
                  : Container(),
              Expanded(
                child: ViewTeam(team: soccerFixture.teams.away),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            decoration: BoxDecoration(
              color: AppColors.lightRed,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Text(
              soccerFixture.fixture.status.long,
              style: TextStyle(
                color: AppColors.white,
                fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildFixtureResult(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              soccerFixture.goals.home.toString(),
              style: TextStyle(
                  color: AppColors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 28),
            ),
            const SizedBox(width: 10),
            Text(
              " - ",
              style: TextStyle(
                  color: AppColors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            const SizedBox(width: 10),
            Text(
              soccerFixture.goals.away.toString(),
              style: TextStyle(
                  color: AppColors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 28),
            ),
          ],
        ),
        const SizedBox(height: 5),
        Text(
          soccerFixture.fixtureLeague.round,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style:
              TextStyle(color: AppColors.white.withOpacity(0.5), fontSize: 12),
        ),
      ],
    );
  }
}
