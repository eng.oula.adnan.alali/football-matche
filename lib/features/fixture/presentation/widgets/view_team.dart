import 'package:flutter/material.dart';

import '../../../../core/domain/entities/teams.dart';
import '../../../../core/utils/app_colors.dart';

///todo
class ViewTeam extends StatelessWidget {
  final Team team;

  const ViewTeam({Key? key, required this.team}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: Colors.white,
          radius: 35,
          child: Image(
            fit: BoxFit.cover,
            width: 50,
            height: 50,
            image: NetworkImage(team.logo),
          ),
        ),
        const SizedBox(height: 10),
        FittedBox(
          child: Text(
            team.name,
            textAlign: TextAlign.center,
            style: TextStyle(color: AppColors.white, fontSize: 18),
          ),
        ),
      ],
    );
  }
}
