part of 'fixture_cubit.dart';

@immutable
abstract class FixtureState {}

class FixtureInitial extends FixtureState {}

class FixtureLineupsLoading extends FixtureState {}

class FixtureLineupsSuccess extends FixtureState {
  final List<Lineup> lineups;

  FixtureLineupsSuccess({required this.lineups});
}

class FixtureLineupsFailure extends FixtureState {
  final String message;

  FixtureLineupsFailure({required this.message});
}
