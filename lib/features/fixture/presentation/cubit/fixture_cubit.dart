import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';

import '../../domain/entities/lineups.dart';
import '../../domain/use_cases/lineups_usecase.dart';

part 'fixture_state.dart';

class FixtureCubit extends Cubit<FixtureState> {
  final LineupsUseCase lineupsUseCase;

  FixtureCubit({
    required this.lineupsUseCase,
  }) : super(FixtureInitial());

  List<Lineup> lineups = [];

  Future<void> getLineups(String fixtureId) async {
    if (lineups.isEmpty) {
      emit(FixtureLineupsLoading());
      final result = await lineupsUseCase(fixtureId);
      result.fold(
        (left) => emit(FixtureLineupsFailure(message: left.message)),
        (right) {
          lineups = right;
          emit(FixtureLineupsSuccess(lineups: right));
        },
      );
    } else {
      emit(FixtureLineupsSuccess(lineups: lineups));
    }
  }
}
