import 'package:football_app/core/utils/app_constants.dart';

import '../../../../core/api/dio_helper.dart';
import '../models/lineups_model.dart';

abstract class FixtureDataSource {
  Future<List<LineupModel>> getLineups(String fixtureId);
}

class FixtureDataSourceImpl implements FixtureDataSource {
  final DioHelper dioHelper;

  FixtureDataSourceImpl({required this.dioHelper});

  @override
  Future<List<LineupModel>> getLineups(String fixtureId) async {
    try {
      final response = await dioHelper.get(
        url: "${AppConstants.FIXTURES}/${AppConstants.LINEUPS}",
        queryParams: {"fixture": fixtureId},
      );
      List<dynamic> result = response.data["response"];
      List<LineupModel> lineups = List<LineupModel>.from(
        result.map((lineup) => LineupModel.fromJson(lineup)),
      );

      return lineups;
    } catch (error) {
      rethrow;
    }
  }
}
