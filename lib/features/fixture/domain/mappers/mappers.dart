import '../../data/models/lineup_team_model.dart';
import '../../data/models/lineups_model.dart';
import '../../data/models/player_model.dart';
import '../entities/lineup_team.dart';
import '../entities/lineups.dart';
import '../entities/player.dart';

extension PlayerExtension on PlayerModel {
  Player toDomain() =>
      Player(id: id, name: name, number: number, grid: grid, pos: pos);
}

extension LineupTeamExtension on LineupTeamModel {
  LineupTeam toDomain() =>
      LineupTeam(id: id, name: name, logo: logo, colors: colors);
}

extension PlayerColorsExtension on PlayerColorsModel {
  PlayerColors toDomain() =>
      PlayerColors(primary: primary, number: number, border: border);
}

extension LineupExtension on LineupModel {
  Lineup toDomain() => Lineup(
        team: team,
        coachName: coachName,
        formation: formation,
        startXI: startXI,
        substitutes: substitutes,
      );
}

extension LineupColorsExtension on LineupColorsModel {
  LineupColors toDomain() =>
      LineupColors(player: player, goalKeeper: goalKeeper);
}
