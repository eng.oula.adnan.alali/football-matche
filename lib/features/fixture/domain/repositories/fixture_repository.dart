import 'package:dartz/dartz.dart';

import '../../../../core/error/error_handler.dart';
import '../entities/lineups.dart';

abstract class FixtureRepository {
  Future<Either<Failure, List<Lineup>>> getLineups(String fixtureId);
}
