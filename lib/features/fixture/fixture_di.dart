import 'package:football_app/di.dart';

import '../../core/api/dio_helper.dart';
import 'data/data_sources/fixture_data_source.dart';
import 'data/repositories/fixture_repository_impl.dart';
import 'domain/use_cases/lineups_usecase.dart';
import 'presentation/cubit/fixture_cubit.dart';

void initFixture() {
  sl.registerLazySingleton<FixtureDataSourceImpl>(
    () => FixtureDataSourceImpl(dioHelper: sl<DioHelper>()),
  );
  sl.registerLazySingleton(
    () => FixtureRepositoryImpl(
      fixtureDataSource: sl<FixtureDataSourceImpl>(),
    ),
  );

  sl.registerLazySingleton(
    () => LineupsUseCase(fixtureRepository: sl<FixtureRepositoryImpl>()),
  );

  sl.registerFactory(
    () => FixtureCubit(
      lineupsUseCase: sl<LineupsUseCase>(),
    ),
  );
}
