abstract class BaseValidator {
  bool validateFunction(String value);

  String getValidateMessage();

  static String? validateValue(
    String value,
    List<BaseValidator> validators,
    bool isValidationActive,
  ) {
    if (!isValidationActive) return null;
    for (int i = 0; i < validators.length; i++) {
      if (!validators[i].validateFunction(value))
        return validators[i].getValidateMessage();
    }
    return null;
  }
}
