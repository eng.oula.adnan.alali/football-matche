import 'package:dio/dio.dart';
import 'package:football_app/di.dart';

import '../utils/app_constants.dart';
import 'interceptors.dart';

class DioHelper {
  final Dio dio;

  DioHelper({required this.dio}) {
    Map<String, dynamic> headers = {
      'Content-Type': 'application/json',
      'x-rapidapi-host': 'v3.football.api-sports.io',
      'x-rapidapi-key': AppConstants.API_KEY,
    };
    dio.options = BaseOptions(
      baseUrl: AppConstants.baseUrl,
      receiveDataWhenStatusError: true,
      receiveTimeout: AppConstants.TIMEOUT,
      connectTimeout: AppConstants.TIMEOUT,
      headers: headers,
    );
    dio.interceptors.add(sl<LogInterceptor>());
    dio.interceptors.add(sl<AppInterceptors>());
  }

  Future<Response> get({
    required String url,
    Map<String, dynamic>? queryParams,
  }) async {
    return await dio.get(url, queryParameters: queryParams);
  }
}
