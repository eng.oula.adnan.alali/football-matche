import 'dart:async';

import 'package:flutter/material.dart';
import 'package:football_app/core/utils/app_assets.dart';
import 'package:football_app/core/utils/app_colors.dart';
import 'package:football_app/core/utils/app_route.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation sizeAnimation;

  @override
  void initState() {
    startTimer(true);
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2200));
    sizeAnimation = Tween<double>(begin: 40.0, end: 200.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.bounceOut));
    super.initState();
  }

  void startTimer(bool back) {
    Timer(Duration(seconds: back != null ? 7 : 7), () {
      Navigator.of(context).pushReplacementNamed(Routes.soccerLayout);
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward();

    return Scaffold(
      backgroundColor: AppColors.white,
      body: Stack(
        children: [
          Center(
            child: AnimatedBuilder(
              animation: sizeAnimation,
              builder: (BuildContext? context, Widget? child) {
                return Image.asset(
                  AppAssets.logo,
                  width: double.infinity,
                  height: double.infinity,
                );
              },
            ),
          ),
          Positioned(
            bottom: 20,
            left: 150,
            child: CircularProgressIndicator(
              color: AppColors.white,
            ),
          ),
        ],
      ),
    );
  }
}
