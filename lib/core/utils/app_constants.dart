import '../../features/soccer/domain/entities/league_of_fixture.dart';

class AppConstants {
  static const String baseUrl = "https://v3.football.api-sports.io/";
  static const int timeOutDuration = 90;
  static const int maxNameLength = 3;

  static const String API_KEY = "f5bcdad4196489ef5c7c3c16c392f8a6";
  static const int TIMEOUT = 20000;

//apis
  static const FIXTURES = "fixtures";
  static const LEAGUES = "leagues";
  static const LINEUPS = "lineups";

  static var availableLeagues = [
    1,
    2,
    3,
    4,
    5,
    6,
    12,
    15,
    20,
    22,
    25,
    40,
    50,
    60,
    61,
    65,
    70,
    75,
    78,
    80,
    90,
    100,
    110,
    120,
    125,
    135,
    140,
  ];
  static Map<int, LeagueOfFixture> leaguesFixtures = {};
}
