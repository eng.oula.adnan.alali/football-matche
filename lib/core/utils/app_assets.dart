const String imagePath = "assets/images";

class AppAssets {
  static const String logo = "$imagePath/logoFootball.jpg";
  static const String playground = "$imagePath/soccer-field.jpg";
}
