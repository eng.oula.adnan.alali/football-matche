class Pair<First, Second> {
  First first;
  Second second;

  Pair({required this.first, required this.second});

  Pair update(Function f) {
    return f();
  }
}
